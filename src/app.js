// define the constants
const screen = document.querySelector('.screen')
const userInput = document.querySelector('[data-userInput]')
const save = document.querySelector('#save')

// console.log(screen, userInput, save)

userInput.addEventListener('keyup', displayText)

function displayText(event) {
  const { value } = event.target;

  screen.textContent = value

  if (_keywords.indexOf(value) > 0) {
    console.log(value)
  }



  // console.log(event)
}

// JAVASCRIPT KEYWORDS
const _keywords = [
  "break", "case",
  "catch",
  "continue",
  "debugger",
  "default",
  "delete",
  "do",
  "else",
  "finally",
  "for",
  "function",
  "if",
  "in",
  "instanceof",
  "new",
  "return",
  "switch",
  "this",
  "throw",
  "try",
  "typeof",
  "var",
  "void",
  "while",
  "with",
  "class",
  "const",
  "enum",
  "export",
  "extends",
  "import",
  "super",
  "implements",
  "interface",
  "let",
  "package",
  "private",
  "protected",
  "public",
  "static",
  "yield",
  "null",
  "true",
  "false",
  "NaN",
  "Infinity",
  "undefined"
]